/**
 * 
 */
package com.fit.earsiv.util;

import java.util.Date;

import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.AttachmentType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentTypeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EmbeddedDocumentBinaryObjectType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;

/**
 * @author yunus.tastutan@fitcons.com
 *
 * Mar 24, 2015  -  7:09:00 AM
 */
public class XsltControlUtil {
	
	public static DocumentReferenceType generateAddDocRef(String invId, byte[] defaultXslt) throws Exception{
		DocumentReferenceType documentReferenceType = new DocumentReferenceType();
		IDType idType = new IDType();
		idType.setValue(invId);

		IssueDateType issueDateType = new IssueDateType();
		issueDateType.setValue(DateUtil.toXmlDate(new Date()));
		
		DocumentTypeType documentTypeType = new DocumentTypeType();
		documentTypeType.setValue("XSLT");

		AttachmentType attachmentType = new AttachmentType();
		EmbeddedDocumentBinaryObjectType binaryObjectType = new EmbeddedDocumentBinaryObjectType();

		binaryObjectType.setValue(defaultXslt);
		binaryObjectType.setCharacterSetCode("UTF-8");
		binaryObjectType.setEncodingCode("Base64");
		binaryObjectType.setFilename(invId + ".xslt");
		binaryObjectType.setMimeCode("application/xml");
		attachmentType.setEmbeddedDocumentBinaryObject(binaryObjectType);

		documentReferenceType.setAttachment(attachmentType);
		documentReferenceType.setID(idType);
		documentReferenceType.setIssueDate(issueDateType);
		documentReferenceType.setDocumentType(documentTypeType);
		return documentReferenceType;
	}

	public static DocumentReferenceType generateAddDocRef(DocumentReferenceType documentReferenceType, String invId, byte[] defaultXslt) {

		AttachmentType attachmentType = new AttachmentType();
		EmbeddedDocumentBinaryObjectType binaryObjectType = new EmbeddedDocumentBinaryObjectType();
//		System.err.println("xuxux>>>defaultXslt=" + defaultXslt.length);
		binaryObjectType.setValue(defaultXslt);
		binaryObjectType.setCharacterSetCode("UTF-8");
		binaryObjectType.setEncodingCode("Base64");
		binaryObjectType.setFilename(invId + ".xslt");
		binaryObjectType.setMimeCode("application/xml");
		attachmentType.setEmbeddedDocumentBinaryObject(binaryObjectType);

		documentReferenceType.setAttachment(attachmentType);
		return documentReferenceType;
	}

}
