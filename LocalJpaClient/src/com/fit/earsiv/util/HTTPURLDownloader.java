package com.fit.earsiv.util;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPURLDownloader {
	private final static int buffer = 1024;

	public static byte[] downloadURL(String urlString) throws Exception {
		ByteArrayOutputStream out = null;
		InputStream in = null;
		byte[] data;
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			in = conn.getInputStream();
			out = new ByteArrayOutputStream();

			byte[] b = new byte[buffer];
			int count;
			while((count = in.read(b)) >= 0) {
				out.write(b, 0, count);
			}
			out.flush();
			// out.close();
			// in.close();
			data = out.toByteArray();

		}
		finally {
			UtilsIO.closeStream(out, in);
		}

		return data;

	}
}
