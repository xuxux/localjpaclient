package com.fit.earsiv.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DomUtil {

	public static Document parse(InputStream is) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory builderFactory;
		DocumentBuilder builder;
		try {
			builderFactory = DocumentBuilderFactory.newInstance();
			builderFactory.setNamespaceAware(true);
			builder = builderFactory.newDocumentBuilder();
			return builder.parse(is);
		}
		finally {
			builder = null;
			builderFactory = null;
		}
	}

	public static Document parse(byte[] data) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory builderFactory;
		DocumentBuilder builder;
		ByteArrayInputStream bis = null;
		try {
			bis = new ByteArrayInputStream(data);
			InputSource source = new InputSource(bis);
			builderFactory = DocumentBuilderFactory.newInstance();
			// fortify
			builderFactory.setExpandEntityReferences(false);
			builderFactory.setNamespaceAware(true);
			builder = builderFactory.newDocumentBuilder();
			return builder.parse(source);
		}
		finally {
			builder = null;
			builderFactory = null;
			UtilsIO.closeStream(bis);
		}
	}

	public static OutputStream transform(Document document, OutputStream os) throws TransformerException {
		TransformerFactory transformerFactory;
		Transformer transformer;
		try {
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.transform(new DOMSource(document), new StreamResult(os));
		}
		finally {
			transformer = null;
			transformerFactory = null;
		}
		return os;
	}

	public static void transform(String inXML, String inXSL, String outTXT) throws TransformerConfigurationException, TransformerException {
		TransformerFactory factory = TransformerFactory.newInstance();
		StreamSource xslStream = new StreamSource(inXSL);
		xslStream.setSystemId(inXSL);

		Transformer transformer = factory.newTransformer(xslStream);

		StreamSource in = new StreamSource(inXML);
		in.setSystemId(inXML);
		StreamResult out = new StreamResult(outTXT);
		transformer.transform(in, out);
	}

	public static Document newDocument() throws ParserConfigurationException {
		DocumentBuilderFactory factory;
		DocumentBuilder builder;
		try {
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			return builder.newDocument();

		}
		finally {
			builder = null;
			factory = null;
		}
	}

	public static Node firstNode(NodeList nodeList) {
		Node node = null;
		if(nodeList != null)
			for(int i = 0; i < nodeList.getLength(); i++) {
				node = nodeList.item(i);
				if(node.getNodeType() != Node.TEXT_NODE)
					return node;
			}
		return node;
	}

	public static Element getXmlRootElem(Document document) {
		return document.getDocumentElement();
	}

	public static Node getFirstNode(NodeList nodeList) {
		for(int i = 0; nodeList != null && i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if(node.getNodeType() != Node.TEXT_NODE)
				return node;
		}
		return null;

	}

	public static String getFirstContext(NodeList nodeList) {
		for(int i = 0; nodeList != null && i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if(node.getNodeType() != Node.TEXT_NODE)
				return node.getTextContent();
		}
		return "";
	}

	public static boolean validateSchema(Document document, String schemaFile) throws SAXException, IOException {
		SchemaFactory factory;
		Schema schema;
		Validator validator;
		factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			schema = factory.newSchema(new File(schemaFile));

			validator = schema.newValidator();
			validator.validate(new DOMSource(document));
			return true;
		}
		finally {
			validator = null;
			schema = null;
			factory = null;
		}
	}

	public static boolean validateZippedXmlsSchema(byte[] zippedXmls, String schemaFilePath) throws IOException, SAXException, ParserConfigurationException {
		byte[] buffer = new byte[1024];
		boolean res = false;
		ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(zippedXmls));
		ZipEntry ze = zis.getNextEntry();
		try {
			while(ze != null) {
				String fileName = ze.getName();
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				int len;
				while((len = zis.read(buffer)) > 0) {
					bos.write(buffer, 0, len);
				}
				res = DomUtil.validateSchema(DomUtil.parse(bos.toByteArray()), schemaFilePath);
				if(!res) {
					return res;
				}
				// bos.close();
				UtilsIO.closeStream(bos);
				ze = zis.getNextEntry();
			}
		}
		finally {
			UtilsIO.closeStream(zis);
		}
		// zis.closeEntry();
		// zis.close();

		return res;
	}

	public static final void prettyPrint(Document xml) throws Exception {
		Writer out = null;
		try {
			Transformer tf = TransformerFactory.newInstance().newTransformer();
			tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			tf.setOutputProperty(OutputKeys.INDENT, "yes");
			out = new StringWriter();
			tf.transform(new DOMSource(xml), new StreamResult(out));
			System.out.println(out.toString());
		}
		finally {
			UtilsIO.closeStream(out);
		}

	}

	public static synchronized void transformToHtmlStaticaly(StreamSource xsl, StreamSource xml, StreamResult result) throws TransformerException {
		TransformerFactory transformerFactory;
		Transformer transformer;
		/**
		 * xuxux - saxon
		 */
		try {
			// xsl.setSystemId("einvoice");
			transformerFactory = new net.sf.saxon.TransformerFactoryImpl();
			transformer = transformerFactory.newTransformer(xsl);
			transformer.transform(xml, result);
		}
		finally {
			transformer = null;
			transformerFactory = null;
		}
	}

	public static String toString(Document doc) {
		StringWriter sw = null;
		try {
			sw = new StringWriter();
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			transformer.transform(new DOMSource(doc), new StreamResult(sw));
			return sw.toString();
		}
		catch (Exception ex) {
			throw new RuntimeException("Error converting to String", ex);
		}
		finally {
			UtilsIO.closeStream(sw);
		}

	}

}
