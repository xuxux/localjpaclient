package com.fit.earsiv.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Hashtable;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;

import org.unece.cefact.namespaces.standardbusinessdocumentheader.StandardBusinessDocument;
import org.xml.sax.SAXException;

import tr.gov.efatura.package_namespace.Package;

import com.fit.earsiv.handler.MyValidationEventHandler;

/**
 * gereksiz kodlar temizlendi
 * 
 * @author yunus.tastutan Mar 8, 2015 - 7:12:19 AM
 */

public class GenericMarshallUnmarshallUtil {

	private static final Hashtable<Class<?>, JAXBContext> contexts;
	private static final Hashtable<Class<?>, String> schemaLocations;

	static {
		contexts = new Hashtable<Class<?>, JAXBContext>();
		schemaLocations = new Hashtable<Class<?>, String>();
		try {
			contexts.put(StandardBusinessDocument.class, JAXBContext.newInstance(StandardBusinessDocument.class, Package.class, InvoiceType.class));
			contexts.put(InvoiceType.class, JAXBContext.newInstance(InvoiceType.class));

			schemaLocations.put(StandardBusinessDocument.class, "http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader PackageProxy.xsd");
			schemaLocations.put(InvoiceType.class, "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 UBL-Invoice-2.1.xsd");
		}
		catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	private static synchronized JAXBContext getContext(final Class<?> declaredType) {
		if(null != declaredType) {
			final JAXBContext context = contexts.get(declaredType);
			if(null != context) {
				return context;
			}
			throw new IllegalArgumentException("Context declaration not found for class:" + declaredType);
		}
		throw new IllegalArgumentException("'declaredType' parameter could not be null");
	}

	public static <T> T unmarshallSBD(Class<T> declaredType, StreamSource source) throws JAXBException {
		Unmarshaller unmarshaller = getContext(StandardBusinessDocument.class).createUnmarshaller();
		return unmarshaller.unmarshal(source, declaredType).getValue();
	}

	public static <T> T unmarshallSBD(Class<T> declaredType, InputStream source) throws JAXBException {
		return unmarshallSBD(declaredType, new StreamSource(source));
	}

	public static <T> T unmarshallSBD(Class<T> declaredType, File source) throws JAXBException {
		return unmarshallSBD(declaredType, new StreamSource(source));
	}

	public static <T> T unmarshallSBD(Class<T> declaredType, Reader source) throws JAXBException {
		return unmarshallSBD(declaredType, new StreamSource(source));
	}

	public static <T> T unmarshall(Class<T> t, String inputXML) throws JAXBException, ClassNotFoundException {
		JAXBContext jc = JAXBContext.newInstance(Class.forName(t.getName()));
		Unmarshaller um = jc.createUnmarshaller();
		Source src = new StreamSource(new StringReader(inputXML));
		JAXBElement<T> jaxbElement = um.unmarshal(src, t);

		return jaxbElement.getValue();
	}

	public static <T> T unmarshall(Class<T> t, byte[] data) throws JAXBException, ClassNotFoundException {
		JAXBElement<T> jaxbElement = null;
		ByteArrayInputStream bis = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(Class.forName(t.getName()));
			Unmarshaller um = jc.createUnmarshaller();
			bis = new ByteArrayInputStream(data);
			Source src = new StreamSource(bis);
			jaxbElement = um.unmarshal(src, t);
		}
		finally {
			UtilsIO.closeStream(bis);
		}

		return jaxbElement.getValue();
	}

	public static <T> T unmarshallWithSchemaValidation(Class<T> t, byte[] data, File schemaFile, MyValidationEventHandler eventHandler) throws JAXBException, ClassNotFoundException, SAXException {
		ByteArrayInputStream bis = null;
		JAXBElement<T> jaxbElement;
		try {
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(schemaFile);
			JAXBContext jc = JAXBContext.newInstance(Class.forName(t.getName()));
			Unmarshaller um = jc.createUnmarshaller();
			um.setSchema(schema);
			um.setEventHandler(eventHandler);
			bis = new ByteArrayInputStream(data);
			Source src = new StreamSource(bis);
			jaxbElement = um.unmarshal(src, t);
		}
		finally {
			UtilsIO.closeStream(bis);
		}

		return jaxbElement.getValue();

	}

	public static <T> T unmarshallSbdWithSchemaValidation(Class<T> t, byte[] data, File schemaFile, MyValidationEventHandler eventHandler) throws JAXBException, ClassNotFoundException, SAXException {
		ByteArrayInputStream bis = null;
		JAXBElement<T> jaxbElement;
		try {
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(schemaFile);
			// JAXBContext jc = JAXBContext.newInstance(Class.forName(t.getName()));
			JAXBContext jc = getContext(StandardBusinessDocument.class);
			Unmarshaller um = jc.createUnmarshaller();
			um.setSchema(schema);
			um.setEventHandler(eventHandler);
			bis = new ByteArrayInputStream(data);
			Source src = new StreamSource(bis);
			jaxbElement = um.unmarshal(src, t);
		}
		finally {
			UtilsIO.closeStream(bis);
		}
		return jaxbElement.getValue();

	}

	public static <T> String marshall(T t) throws JAXBException {
		StringWriter sw = null;
		String str = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(t.getClass());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			// m.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
			sw = new StringWriter();
			m.marshal(t, sw);
			str = sw.toString();
		}
		finally {
			UtilsIO.closeStream(sw);
		}
		return str;
	}
	
	  public static <T> String marshallOLD(T t) throws JAXBException {
		    JAXBContext jc = JAXBContext.newInstance(t.getClass());
		    Marshaller m = jc.createMarshaller();
		    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		    // m.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
		    StringWriter sw = new StringWriter();
		    m.marshal(t, sw);
		    return sw.toString();
		  }

	public static <T> String marshallEArchiveReport(T t) throws JAXBException {
		String str;
		StringWriter sw = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(t.getClass());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://earsiv.efatura.gov.tr eArsiv.xsd");
			sw = new StringWriter();
			m.marshal(t, sw);
			str = sw.toString();
		}
		finally {
			UtilsIO.closeStream(sw);
		}

		return str;
	}

	public static synchronized <T> String marshallQuickly(T t) throws JAXBException {
		StringWriter sw = null;
		String str;
		try {
			JAXBContext jc = getContext(t.getClass());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			// m.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
			sw = new StringWriter();
			m.marshal(t, sw);

			str = sw.toString();
		}
		finally {
			UtilsIO.closeStream(sw);
		}

		return str;
	}

	public static <T> T unmarshallQuickly(Class<T> t, byte[] data) throws JAXBException, ClassNotFoundException {
		ByteArrayInputStream bis = null;
		JAXBElement<T> jaxbElement;
		try {
			JAXBContext jc = getContext(t);
			Unmarshaller um = jc.createUnmarshaller();
			bis = new ByteArrayInputStream(data);
			Source src = new StreamSource(bis);
			jaxbElement = um.unmarshal(src, t);
		}
		finally {
			UtilsIO.closeStream(bis);
		}


		return jaxbElement.getValue();
	}

	public static <T> T unmarshallSbdWithoutSchemaValidation(Class<T> t, byte[] data) throws JAXBException, ClassNotFoundException, SAXException {
		ByteArrayInputStream bis = null;
		JAXBElement<T> jaxbElement;
		try {
			// JAXBContext jc = JAXBContext.newInstance(Class.forName(t.getName()));
			JAXBContext jc = getContext(StandardBusinessDocument.class);
			Unmarshaller um = jc.createUnmarshaller();

			bis = new ByteArrayInputStream(data);
			Source src = new StreamSource(bis);
			jaxbElement = um.unmarshal(src, t);
		}
		finally {
			UtilsIO.closeStream(bis);
		}


		return jaxbElement.getValue();

	}

	public static <T> String marshallSbd(T t) throws JAXBException {
		StringWriter sw = null;
		String str;
		try {
			JAXBContext jc = getContext(StandardBusinessDocument.class);
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader PackageProxy_1_2.xsd");
			// m.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
			sw = new StringWriter();
			m.marshal(t, sw);
			str = sw.toString();
		}
		finally {
			UtilsIO.closeStream(sw);
		}

		return str;
	}

}
