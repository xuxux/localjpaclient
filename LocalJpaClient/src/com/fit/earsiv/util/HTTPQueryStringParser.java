package com.fit.earsiv.util;

import org.apache.commons.lang3.StringUtils;

public class HTTPQueryStringParser {

  
  private static final long serialVersionUID = 5692074393964847657L;
  
  private static final String P_PARAM_SEPARATOR = "&";
  private static final String P_VALUE_SEPARATOR = "=";
  
  private static final String P_REMOTE_IP = "IP";
  private static final String P_SOURCE = "source";
  //private static final String P_USERNAME = "username";
  //private static final String P_PASSWORD = "password";
  
  private String remoteIP;
  private String source;
  //private String username;
  //private String password;
  
  private HTTPQueryStringParser() {};
  
  public static HTTPQueryStringParser newInstance(String queryString) {
      HTTPQueryStringParser parser = new HTTPQueryStringParser();
      if(StringUtils.isEmpty(queryString)) {
          return parser;
      }
      
      String[] paramPairs = queryString.split(P_PARAM_SEPARATOR);
      for(String paramPair : paramPairs) {
          if(!StringUtils.isEmpty(paramPair)) {
              String[] paramValue = paramPair.split(P_VALUE_SEPARATOR);
              if(paramValue.length == 2) {
                  String param = paramValue[0];
                  String value = paramValue[1];
                  
                  if(StringUtils.equalsIgnoreCase(param, P_REMOTE_IP)) {
                      parser.remoteIP = value;
                  }
                  else if(StringUtils.equalsIgnoreCase(param, P_SOURCE)) {
                      parser.source = value;
                  }
                  /*
                  else if(StringUtils.equalsIgnoreCase(param, P_USERNAME)) {
                      parser.username = value;
                  }
                  else if(StringUtils.equalsIgnoreCase(param, P_PASSWORD)) {
                      parser.password = value;
                  }
                  */
              }
          }
      }
      return parser;
  }
  
  public static void main(String[] args) {
      String q = "ip=123.123.123.123&source=SAP";
      HTTPQueryStringParser parser = HTTPQueryStringParser.newInstance(q);
      System.out.println(parser.remoteIP + " - " + parser.source);
  }

  @Override
  public String toString() {
      return "[IP: '" + remoteIP + "', Source: '" + source + "']";
      //return "[IP: '" + remoteIP + "', Source: '" + source + "', Username: '" + username + "', Password: '" + password + "']";
  }
  
  public String getRemoteIP() {
      return remoteIP;
  }

  public void setRemoteIP(String remoteIP) {
      this.remoteIP = remoteIP;
  }

  public String getSource() {
      return source;
  }

  public void setSource(String source) {
      this.source = source;
  }
  /*
  public String getUsername() {
      return username;
  }

  public void setUsername(String username) {
      this.username = username;
  }

  public String getPassword() {
      return password;
  }

  public void setPassword(String password) {
      this.password = password;
  }
  */


}
