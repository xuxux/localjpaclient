package com.fit.earsiv.util;

import java.awt.Dimension;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zefer.pd4ml.PD4Constants;
import org.zefer.pd4ml.PD4ML;

public class XmlConverter {

	public static void main(String[] args) throws Exception {
		String filename = "Apple_Retail";
		byte[] zip = FileUtils.readFileToByteArray(new File("C:\\Temp\\" + filename + ".zip"));
		byte[] pdf = toPdf(zip, null);
		FileUtils.writeByteArrayToFile(new File("C:\\Temp\\" + filename + ".pdf"), pdf);
	}

	public static synchronized byte[] toPdf(byte[] bs, String xslt) throws Exception {
		byte[] hmtlBytes = null;
		if(StringUtil.isNullOrEmpty(xslt)) {
			hmtlBytes = toHtml(bs);
		}
		else {
			hmtlBytes = toHtml(bs, xslt);
		}

		if(null == hmtlBytes) {
			throw new Exception("Could not generate html output!");
		}
		// Converting from HTML TO PDF
		ByteArrayOutputStream bos = null;
		InputStreamReader inr = null;
		try {
			PD4ML pd4ml = new PD4ML();
			Dimension landscapeA4 = pd4ml.changePageOrientation(PD4Constants.A4);
			pd4ml.setPageSize(landscapeA4);
			pd4ml.setPageInsets(new java.awt.Insets(0, 0, 0, 0));
			/**
			 * TODO: turkuvaz için kaldırıldı - 04.12.2015
			 */
			// pd4ml.addStyle("BODY{margin:0;padding:0}", true);
			pd4ml.setPageSizeMM(new Dimension(210, 297));
			pd4ml.debug = false;
			pd4ml.setHtmlWidth(1000);
			pd4ml.useTTF("java:fonts", true);
			pd4ml.setAuthorName("FIT");

			bos = new ByteArrayOutputStream();
			inr = new InputStreamReader(new ByteArrayInputStream(hmtlBytes), "UTF-8");
			pd4ml.render(inr, bos);

			return bos.toByteArray();
		}
		catch (Exception e) {
			// System.err.println(StringUtil.parseError(e));
			throw e;
		}
		finally {
			try {
				if(bos != null)
					bos.close();
			}
			catch (Exception e2) {}
			bos = null;
			try {
				if(inr != null)
					inr.close();
			}
			catch (Exception e) {
				// System.err.println(StringUtil.parseError(e));
				UtilsIO.handleTrivialException(XmlConverter.class, e);
			}
			inr = null;
		}
	}

	public static byte[] toHtml(byte[] bs, String xslt) {
		if(xslt == null) {
			throw new NullPointerException("default XSLT is null, output will not be generated!");
		}
		ByteArrayOutputStream os = null;
		ZipInputStream zis = null;
		try {
			os = new ByteArrayOutputStream();
			byte[] data;
			zis = new ZipInputStream(new ByteArrayInputStream(bs));

			ZipEntry zipEntry = null;
			byte[] unzippedData = null;
			try {
				while((zipEntry = zis.getNextEntry()) != null) {
					unzippedData = ZipUtil.extractZipEntry(zis);
				}
				zis.closeEntry();
				zis.close();
			}
			finally {
				if(zis != null)
					zis.close();
				zis = null;
			}

			ByteArrayInputStream bais = new ByteArrayInputStream(BOMUtil.removeBOM(CryptoUtil.commonsDecodeBase64byteArray(xslt), BOMUtil.UTF8_BOM));
			InputStreamReader reader = new InputStreamReader(bais, "UTF-8");

			DomUtil.transformToHtmlStaticaly(new StreamSource(reader), new StreamSource(new ByteArrayInputStream(unzippedData)), new StreamResult(os));
			data = os.toByteArray();
			return data;

		}
		catch (FileNotFoundException e1) {
			// System.err.println(StringUtil.parseError(e1));
			UtilsIO.handleTrivialException(XmlConverter.class, e1);
		}
		catch (IOException e1) {
			// System.err.println(StringUtil.parseError(e1));
			UtilsIO.handleTrivialException(XmlConverter.class, e1);
		}
		catch (TransformerException e) {
			// System.err.println(StringUtil.parseError(e));
			UtilsIO.handleTrivialException(XmlConverter.class, e);
		}
		finally {
			// os.close();
			UtilsIO.closeStream(os, zis);
		}
		return null;
	}

	public static byte[] toHtml(byte[] bs) {
		ByteArrayOutputStream os = null;
		ZipInputStream zis = null;
		Document document = null;
		ByteArrayInputStream bais = null;
		InputStreamReader reader = null;
		try {
			os = new ByteArrayOutputStream();
			zis = new ZipInputStream(new ByteArrayInputStream(bs));

			ZipEntry zipEntry = null;
			byte[] unzippedData = null;
			try {
				while((zipEntry = zis.getNextEntry()) != null) {
					unzippedData = ZipUtil.extractZipEntry(zis);
				}
				zis.closeEntry();
				zis.close();
			}
			finally {
				if(zis != null)
//					zis.close();
					UtilsIO.closeStream(zis);
				zis = null;
			}

			document = DomUtil.parse(unzippedData);

			NodeList addDocRefs = document.getElementsByTagNameNS("*", "AdditionalDocumentReference");
			String xslt = null;
			if(addDocRefs != null) {
				for(int i = 0; i < addDocRefs.getLength(); i++) {
					Element addDocRef = (Element) addDocRefs.item(i);
					if(addDocRef != null) {
						Element docType = (Element) addDocRef.getElementsByTagNameNS("*", "DocumentType").item(0);
						if(docType != null && "XSLT".equalsIgnoreCase(docType.getTextContent())) {
							Element attach = (Element) addDocRef.getElementsByTagNameNS("*", "Attachment").item(0);
							if(attach != null) {
								xslt = attach.getElementsByTagNameNS("*", "EmbeddedDocumentBinaryObject").item(0).getTextContent();
							}
						}
					}
				}
			}

			if(xslt == null) {
				throw new NullPointerException("XSLT does not found in the invoice, output will not be generated!");
			}

			bais = new ByteArrayInputStream(BOMUtil.removeBOM(CryptoUtil.commonsDecodeBase64byteArray(xslt), BOMUtil.UTF8_BOM));
			reader = new InputStreamReader(bais, "UTF-8");

			DomUtil.transformToHtmlStaticaly(new StreamSource(reader), new StreamSource(new ByteArrayInputStream(unzippedData)), new StreamResult(os));

			return os.toByteArray();

		}
		catch (FileNotFoundException e1) {
			// System.err.println("Error when converting to html with inner xslt: " + StringUtil.traceException(e1));
			UtilsIO.handleTrivialException(XmlConverter.class, e1);
		}
		catch (ParserConfigurationException e1) {
			// System.err.println("Error when converting to html with inner xslt: " + StringUtil.traceException(e1));
			UtilsIO.handleTrivialException(XmlConverter.class, e1);
		}
		catch (SAXException e1) {
			// System.err.println("Error when converting to html with inner xslt: " + StringUtil.traceException(e1));
			UtilsIO.handleTrivialException(XmlConverter.class, e1);
		}
		catch (IOException e1) {
			// System.err.println("Error when converting to html with inner xslt: " + StringUtil.traceException(e1));
			UtilsIO.handleTrivialException(XmlConverter.class, e1);
		}
		catch (TransformerException e) {
			// System.err.println("Error when converting to html with inner xslt: " + StringUtil.traceException(e));
			UtilsIO.handleTrivialException(XmlConverter.class, e);
		}
		catch (Exception e) {
			// System.err.println("Error when converting to html with inner xslt: " + StringUtil.traceException(e));
			UtilsIO.handleTrivialException(XmlConverter.class, e);
		}
		finally {
			// try {
			// os.close();
			// } catch (IOException e) {
			// // System.err.println(StringUtil.parseError(e));
			// }
			UtilsIO.closeStream(os, zis, bais, reader);
		}
		return null;
	}

}
