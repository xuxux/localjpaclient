package com.fit.earsiv.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

/**
 * 
 * @author xuxux
 * 
 */

public class CryptoUtil {

	private static final Base64 BASE64 = new Base64();

	public static final String SALT = "gofitgo";

	public static void main(String[] args) throws Exception {
		byte[] data = FileUtils.readFileToByteArray(new File("C:\\Users\\muratdemir\\Desktop\\error\\EA02015000000001.xml"));
		System.out.println(sha256(data));
	}

	public static String md5(byte[] value) throws NoSuchAlgorithmException {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.update(value, 0, value.length);
			BigInteger i = new BigInteger(1, digest.digest());
			return String.format("%1$032X", i);
		}
		finally {
			digest = null;
		}
	}

	public static byte[] decodeHex(char[] data) {
		int len = data.length;
		if((len & 0x1) != 0)
			return new byte[0];

		byte[] out = new byte[len >> 1];

		int i = 0;
		for(int j = 0; j < len; i++) {
			int f = Character.digit(data[j], 16) << 4;
			j++;
			f |= Character.digit(data[j], 16);
			j++;
			out[i] = ((byte) (f & 0xFF));
		}

		return out;
	}

	public static String byteArrayToHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for(int i = 0; i < b.length; i++) {
			int v = b[i] & 0xff;
			if(v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}

	public static String commonsEncodeBase64(byte[] value) throws UnsupportedEncodingException {
		byte[] encodedBytes = BASE64.encode(value);
		return new String(encodedBytes, "UTF-8");
	}

	public static String commonsDecodeBase64(String value) throws UnsupportedEncodingException {
		byte[] decodedBytes = BASE64.decode(value);
		return new String(decodedBytes, "UTF-8");
	}

	public static byte[] commonsDecodeBase64byteArray(String value) throws UnsupportedEncodingException {
		return BASE64.decode(value);
	}

	public static String sha256(byte[] value) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		}
		catch (NoSuchAlgorithmException e) {
			// System.err.println(StringUtil.traceException(e));
			UtilsIO.handleTrivialException(CryptoUtil.class, e);
		}
		md.update(value);

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();

	}
}
