package com.fit.earsiv.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * 
 * @author murat.demir
 * 
 */
public class DateUtil {

  private static final TimeZone timeZone = TimeZone.getTimeZone("Turkey");
  private static final Locale locale = new Locale("tr_TR");

  public static final SimpleDateFormat FORMAT_SHORT_STAMP = new SimpleDateFormat("yyyy-MM");
  public static final SimpleDateFormat FORMAT_SHORT = new SimpleDateFormat("yyyy-MM-dd");
  public static final SimpleDateFormat FORMAT_FULL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z	");
  public static final SimpleDateFormat FORMAT_MID = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

  private final static SimpleDateFormat DATE_FORMAT_SIMPLE = new SimpleDateFormat("yyyyMMdd");

  private final static SimpleDateFormat DATE_FORMAT_SIMPLE_REVERSE = new SimpleDateFormat("yyyyMMdd");

  static {
    TimeZone.setDefault(timeZone);
    FORMAT_SHORT.setTimeZone(timeZone);
    FORMAT_SHORT.setLenient(false);
    FORMAT_FULL.setTimeZone(timeZone);
    FORMAT_MID.setTimeZone(timeZone);
  }

  private DateUtil() {}

  public static void main(String[] args) throws Exception {

    int i = 11;
    Date waiting_day_kadar_oncesi = DateUtil.getPreviousDate(DateUtil.localeDate(), i);
    System.out.println("Eylül " + endOfMonth(waiting_day_kadar_oncesi));
    Date date = getPreviousMonth(waiting_day_kadar_oncesi);
    System.out.println("Agustos " + endOfMonth(date));
    Date date2 = getPreviousMonth(date);
    System.out.println("Temmuz " + endOfMonth(date2));
    for (int j = 0; j < 2; j++) {
      System.out.println(j);
    }
  }

  public static Date getPreviousDate(Date date, int index) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE, -(Math.abs(index)));
    return calendar.getTime();
  }

  public static Calendar getPreviousDate(Calendar date, int index) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -(Math.abs(index)));
    return calendar;
  }

  public static Date getYesterday(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE, -1);
    return calendar.getTime();
  }

  public static Date parseDate(String value) throws Exception {
    Date date = null;
    if (null != value) {
      try {
        date = DATE_FORMAT_SIMPLE.parse(checkCorrectDateFormat(value), new ParsePosition(0));
      } catch (Throwable e) {
        try {
          date = DATE_FORMAT_SIMPLE_REVERSE.parse(checkCorrectDateFormatReverse(value), new ParsePosition(0));
        } catch (Exception e2) {
          throw new Exception(StringUtil.formatString("Date convertion error for [{0}]", value));
        }
      }
      if (null == date)
        throw new Exception(StringUtil.formatString("Date convertion error for [{0}]", value));
    }
    return date;
  }

  public static Date parseDate(SimpleDateFormat format, String value) throws Exception {
    Date date = null;
    if (!StringUtil.isNullOrEmpty(value)) {
      try {
        date = format.parse(value);
      } catch (Exception e) {
        throw new Exception(StringUtil.formatString("Date convertion error for [{0}] (parse error)", value));
      }
      if (null == date)
        throw new Exception(StringUtil.formatString("Date convertion error for [{0}] (null date)", value));
    } else {
      throw new Exception(StringUtil.formatString("Date convertion error for [{0}] (null value)", value));
    }
    return date;
  }

  public static java.sql.Date toSQLDate(Date date) {
    return new java.sql.Date(date.getTime());
  }

  private static String checkCorrectDateFormat(String value) throws Exception {
    if (value.contains("-")) {
      if (value.indexOf("-") == 4 && value.lastIndexOf("-") == 7) {
        return value.replace("-", "");
      }
      throw new Exception();
    }
    return value;
  }

  private static String checkCorrectDateFormatReverse(String value) throws Exception {
    if (value.contains("-")) {
      if (value.indexOf("-") == 2 && value.lastIndexOf("-") == 5) {
        return value.replace("-", "");
      }
      throw new Exception();
    }
    return value;
  }

  public static TimeZone getTimeZone() {
    return timeZone;
  }

  public static Date toDate(XMLGregorianCalendar calendar) {
    if (calendar == null) {
      return null;
    }
    return calendar.toGregorianCalendar().getTime();
  }

  public static Date toDateWithCurrentTime(XMLGregorianCalendar calendar) throws DatatypeConfigurationException {
    if (calendar == null) {
      return null;
    }
    GregorianCalendar gregorianCalendar = new GregorianCalendar();
    DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
    XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
    calendar.setHour(now.getHour());
    calendar.setMinute(now.getMinute());
    calendar.setSecond(now.getSecond());
    calendar.setMillisecond(now.getMillisecond());
    return calendar.toGregorianCalendar().getTime();
  }

  public static Date toDateWithGivenTime(XMLGregorianCalendar calendar, XMLGregorianCalendar issueTime) throws DatatypeConfigurationException {
    if (calendar == null) {
      return null;
    }
    calendar.setHour(issueTime.getHour());
    calendar.setMinute(issueTime.getMinute());
    calendar.setSecond(issueTime.getSecond());
    calendar.setMillisecond(issueTime.getMillisecond());
    return calendar.toGregorianCalendar().getTime();
  }

  public static Calendar getPreviousMonth(Calendar calendar) {
    calendar.add(Calendar.MONTH, -1);

    int min = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
    calendar.set(Calendar.DAY_OF_MONTH, min);
    return calendar;
  }

  public static Date getPreviousMonth(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.MONTH, -1);

    int min = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
    calendar.set(Calendar.DAY_OF_MONTH, min);

    return calendar.getTime();
  }

  public static Calendar startOfMonth(Calendar cal) {
    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(cal.getTimeInMillis());

    c.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    c.set(Calendar.AM_PM, Calendar.AM);
    return c;
  }

  public static Date startOfMonth(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal.getTime();
  }

  public static Calendar endOfMonth(Calendar cal) {
    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    cal.set(Calendar.HOUR, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal;
  }

  public static Date endOfMonth(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    cal.set(Calendar.HOUR, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal.getTime();
  }

  public static Calendar startOfDate(Calendar cal) {
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal;
  }



  public static Date startOfMinute(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal.getTime();
  }

  public static Date startOfDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal.getTime();
  }


  public static Date endOfDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(Calendar.HOUR, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal.getTime();
  }

  public static Date endOfMinute(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal.getTime();
  }


  public static Calendar endOfDate(Calendar cal) {
    cal.set(Calendar.HOUR, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 999);
    cal.set(Calendar.AM_PM, Calendar.AM);
    return cal;
  }


  public static String formatShortStamp(Date date) {
    return FORMAT_SHORT_STAMP.format(date);
  }

  public static String formatShort(Date date) {
    return FORMAT_SHORT.format(date);
  }

  public static String formatShort(XMLGregorianCalendar date) {
    if (date == null)
      return null;
    return FORMAT_SHORT.format(toDate(date));
  }

  public static String formatFull(Date date) {
    return FORMAT_FULL.format(date);
  }
  
  public static String formatMid(Date date) {
	    return FORMAT_MID.format(date);
	  }

  public static String formatFull(XMLGregorianCalendar date) {
    return FORMAT_FULL.format(toDate(date));
  }

  public static Date currentAddDay(int amount) {
    Calendar cal = localeCalendar();
    cal.add(Calendar.DATE, amount);
    return cal.getTime();
  }

  public static Date currentAddHour(int amount) {
    Calendar cal = localeCalendar();
    cal.add(Calendar.HOUR, amount);
    return cal.getTime();
  }

  public static Date currentAddSecond(int amount) {
    Calendar cal = localeCalendar();
    cal.add(Calendar.SECOND, amount);
    return cal.getTime();
  }

  public static int dateDiff(Date startDate, Date endDate) {
    return Math.abs((int) ((startDate.getTime() - endDate.getTime()) / (24 * 60 * 60 * 1000)));
  }

  public static int dateDiffSeconds(Date startDate, Date endDate) {
    return Math.abs((int) ((startDate.getTime() - endDate.getTime()) / (1000)));
  }

  public static XMLGregorianCalendar xmlDateTime() throws DatatypeConfigurationException {
    GregorianCalendar calendar = new GregorianCalendar();
    XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
    xmlCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
    return xmlCalendar;
  }

  public static XMLGregorianCalendar xmlDate() throws DatatypeConfigurationException {
    XMLGregorianCalendar xmlCalendar = xmlDateTime();
    xmlCalendar.setHour(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMinute(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setSecond(DatatypeConstants.FIELD_UNDEFINED);
    return xmlCalendar;
  }

  public static XMLGregorianCalendar xmlTime() throws DatatypeConfigurationException {
    XMLGregorianCalendar xmlCalendar = xmlDateTime();
    xmlCalendar.setYear(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMonth(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setDay(DatatypeConstants.FIELD_UNDEFINED);
    return xmlCalendar;
  }

  public static Calendar localeCalendar() {
    Calendar calendar = Calendar.getInstance();

    GregorianCalendar cal = new GregorianCalendar(timeZone, locale);
    cal.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
    cal.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
    cal.set(Calendar.DATE, calendar.get(Calendar.DATE));
    cal.set(Calendar.HOUR, calendar.get(Calendar.HOUR));
    cal.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
    cal.set(Calendar.SECOND, calendar.get(Calendar.SECOND));
    cal.set(Calendar.MILLISECOND, calendar.get(Calendar.MILLISECOND));
    cal.set(Calendar.AM_PM, calendar.get(Calendar.AM_PM));

    return cal;
  }

  public static Date localeDate() {
    return localeCalendar().getTime();
  }

  public static java.sql.Date toSqlDate(Date date) {
    return new java.sql.Date(startOfDate(date).getTime());
  }

  public static java.sql.Timestamp toSqlTimestamp(Date date) {
    return new java.sql.Timestamp(date.getTime());
  }

  public static XMLGregorianCalendar toXmlIssueTime(Date date) throws DatatypeConfigurationException {
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
    xmlCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setYear(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setDay(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMonth(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
    return xmlCalendar;
  }

  public static XMLGregorianCalendar toXmlDateTime(Date date) throws DatatypeConfigurationException {
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
    xmlCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
    return xmlCalendar;
  }

  public static XMLGregorianCalendar toXmlDate(Date date) throws DatatypeConfigurationException {
    XMLGregorianCalendar xmlCalendar = toXmlDateTime(date);
    xmlCalendar.setHour(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMinute(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setSecond(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
    return xmlCalendar;
  }

  public static XMLGregorianCalendar toXmlTime(Date date) throws DatatypeConfigurationException {
    XMLGregorianCalendar xmlCalendar = toXmlDateTime(date);
    xmlCalendar.setDay(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setMonth(DatatypeConstants.FIELD_UNDEFINED);
    xmlCalendar.setYear(DatatypeConstants.FIELD_UNDEFINED);
    return xmlCalendar;
  }

  public static boolean isPastDay(Date customerDate) {
    Calendar customerCalendar = Calendar.getInstance();
    customerCalendar.setTime(customerDate);

    Calendar systemCalendar = Calendar.getInstance();
    systemCalendar.setTime(localeDate());

    int customerYear = customerCalendar.get(Calendar.YEAR);
    int customerMonth = customerCalendar.get(Calendar.MONTH);
    int customerDay = customerCalendar.get(Calendar.DAY_OF_MONTH);

    int systemYear = systemCalendar.get(Calendar.YEAR);
    int systemMonth = systemCalendar.get(Calendar.MONTH);
    int systemDay = systemCalendar.get(Calendar.DAY_OF_MONTH);

    if (customerYear < systemYear) {
      return true;
    } else if (customerYear <= systemYear && customerMonth < systemMonth) {
      return true;
    } else if (customerYear <= systemYear && customerMonth <= systemMonth && customerDay < systemDay) {
      return true;
    }
    return false;
  }

  public static boolean isPastDay(XMLGregorianCalendar customerDate) throws Exception {
    try {
      int customerYear = customerDate.getYear();
      int customerMonth = customerDate.getMonth();
      int customerDay = customerDate.getDay();

      GregorianCalendar c = new GregorianCalendar();
      c.setTime(localeDate());
      XMLGregorianCalendar systemCalendarGregorian = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

      int systemYear = systemCalendarGregorian.getYear();
      int systemMonth = systemCalendarGregorian.getMonth();
      int systemDay = systemCalendarGregorian.getDay();

      if (customerYear < systemYear) {
        return true;
      } else if (customerYear <= systemYear && customerMonth < systemMonth) {
        return true;
      } else if (customerYear <= systemYear && customerMonth <= systemMonth && customerDay < systemDay) {
        return true;
      }
    } catch (Exception e) {
      throw e;
    }
    return false;
  }

  public static boolean hasTime(Date date) {
    if (date == null) {
      return false;
    }
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    if (c.get(Calendar.HOUR_OF_DAY) > 0) {
      return true;
    }
    if (c.get(Calendar.MINUTE) > 0) {
      return true;
    }
    if (c.get(Calendar.SECOND) > 0) {
      return true;
    }
    if (c.get(Calendar.MILLISECOND) > 0) {
      return true;
    }
    return false;
  }

  public static Date max(Date d1, Date d2) {
    if (d1 == null && d2 == null)
      return null;
    if (d1 == null)
      return d2;
    if (d2 == null)
      return d1;
    return (d1.after(d2)) ? d1 : d2;
  }

  public static Date min(Date d1, Date d2) {
    if (d1 == null && d2 == null)
      return null;
    if (d1 == null)
      return d2;
    if (d2 == null)
      return d1;
    return (d1.before(d2)) ? d1 : d2;
  }

  public static Date getStart(Date date) {
    return clearTime(date);
  }

  public static boolean isSameDay(Date date1, Date date2) {
    if (date1 == null || date2 == null) {
      throw new IllegalArgumentException("The dates must not be null");
    }
    Calendar cal1 = Calendar.getInstance();
    cal1.setTime(date1);
    Calendar cal2 = Calendar.getInstance();
    cal2.setTime(date2);
    return isSameDay(cal1, cal2);
  }

  public static boolean isSameDay(Calendar cal1, Calendar cal2) {
    if (cal1 == null || cal2 == null) {
      throw new IllegalArgumentException("The dates must not be null");
    }
    return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2
        .get(Calendar.DAY_OF_YEAR));
  }

  public static boolean isToday(Date date) {
    return isSameDay(date, Calendar.getInstance().getTime());
  }

  public static boolean isToday(Calendar cal) {
    return isSameDay(cal, Calendar.getInstance());
  }

  public static boolean isBeforeDay(Date date1, Date date2) {
    if (date1 == null || date2 == null) {
      throw new IllegalArgumentException("The dates must not be null");
    }
    Calendar cal1 = Calendar.getInstance();
    cal1.setTime(date1);
    Calendar cal2 = Calendar.getInstance();
    cal2.setTime(date2);
    return isBeforeDay(cal1, cal2);
  }

  public static boolean isBeforeDay(Calendar cal1, Calendar cal2) {
    if (cal1 == null || cal2 == null) {
      throw new IllegalArgumentException("The dates must not be null");
    }
    if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA))
      return true;
    if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA))
      return false;
    if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR))
      return true;
    if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR))
      return false;
    return cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR);
  }

  public static boolean isAfterDay(Date date1, Date date2) {
    if (date1 == null || date2 == null) {
      throw new IllegalArgumentException("The dates must not be null");
    }
    Calendar cal1 = Calendar.getInstance();
    cal1.setTime(date1);
    Calendar cal2 = Calendar.getInstance();
    cal2.setTime(date2);
    return isAfterDay(cal1, cal2);
  }

  public static boolean isAfterDay(Calendar cal1, Calendar cal2) {
    if (cal1 == null || cal2 == null) {
      throw new IllegalArgumentException("The dates must not be null");
    }
    if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA))
      return false;
    if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA))
      return true;
    if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR))
      return false;
    if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR))
      return true;
    return cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR);
  }

  public static boolean isWithinDaysFuture(Date date, int days) {
    if (date == null) {
      throw new IllegalArgumentException("The date must not be null");
    }
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    return isWithinDaysFuture(cal, days);
  }

  public static boolean isWithinDaysFuture(Calendar cal, int days) {
    if (cal == null) {
      throw new IllegalArgumentException("The date must not be null");
    }
    Calendar today = Calendar.getInstance();
    Calendar future = Calendar.getInstance();
    future.add(Calendar.DAY_OF_YEAR, days);
    return (isAfterDay(cal, today) && !isAfterDay(cal, future));
  }

  public static Date clearTime(Date date) {
    if (date == null) {
      return null;
    }
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTime();
  }

  public static Calendar dateToCalendar(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    return cal;
  }


  /** yunus - 10.08.2016 - raporlama dönemine uygun tarihli fatura/iptal kontrolü **/
  public static boolean isIssuedateInReportPeriod(Date issueDate, Date now, int lastReportDay) {
    try {
      Calendar issueDateCal = DateUtil.dateToCalendar(issueDate);
      Calendar nowCal = DateUtil.dateToCalendar(now);

      if (nowCal.get(Calendar.DAY_OF_MONTH) <= lastReportDay) {
        // bir önceki ayın 1 inden itibaren kesebilir.
        if (issueDateCal.compareTo(DateUtil.getPreviousMonth(DateUtil.startOfMonth(nowCal))) >= 0) {
          // bu durumda sorun yok - bir önceki ayın 1 i ve sonrası
          return true;
        } else {
          // bu durumda sorun var - bir önceki ayın 1 inden de öncesi
          return false;
        }
      } else {
        // bu ayın 1 inden itibaren kesebilir.
        if (issueDateCal.compareTo(DateUtil.startOfMonth(nowCal)) >= 0) {
          // bu durumda sorun yok - bu ayın 1 i ve sonrası
          return true;
        } else {
          // bu durumda sorun var - bu ayın 1 inden öncesi
          return false;
        }
      }
    } catch (Exception e) {
      return false;
    }
  }

  public static Date pickMostRecentDate(Date... dates) {
    Date mostRecent = dates[0];
    for (Date date : dates) {
      if (date.compareTo(mostRecent) > 0) {
        mostRecent = date;
      }
    }
    return mostRecent;
  }

  public static Date pickOldestDate(Date... dates) {
    Date oldestDate = dates[0];
    for (Date date : dates) {
      if (date.compareTo(oldestDate) < 0) {
        oldestDate = date;
      }
    }
    return oldestDate;
  }
  
  public static boolean compareToDates(Date date1, Date date2) {
    
      if (date1.compareTo(date2) < 0) {
        return true;
      }else {
        return false;
      }
  }
}
