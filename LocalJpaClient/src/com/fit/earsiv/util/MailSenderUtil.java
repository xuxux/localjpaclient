package com.fit.earsiv.util;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class MailSenderUtil {

  private String smtpHost;
  private int smtpPort;
  private String username;
  private String password;
  private String sender;
  private String recipient;
  private String bcc;
  private String cc;
  private String subject;
  private String content;
  private byte[] pdf;
  private String pdfName;
  private byte[] xml;
  private String xmlName;
  private boolean useTTL;
  private boolean auth;

  public MailSenderUtil() {}

  public void send() throws MessagingException, UnsupportedEncodingException, Exception {
    Properties properties = System.getProperties();
    properties.setProperty("mail.smtp.host", smtpHost);
    properties.setProperty("mail.smtp.ssl.trust", smtpHost);
    properties.put("mail.smtp.port", smtpPort);
    properties.put("mail.smtp.connectiontimeout", "5000");
    properties.put("mail.smtp.timeout", "9500");

    if (useTTL) {
//      properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      properties.put("mail.smtp.socketFactory.fallback", "false");
      properties.put("mail.smtp.starttls.enable", "true");
      properties.put("mail.smtp.ssl.trust", smtpHost);
    }else{
      properties.put("mail.smtp.starttls.enable", "false");
    }

    Session session = null;
    if (auth) {
      properties.put("mail.smtp.auth", "true");
//      properties.put("mail.smtp.auth", true);
      session = Session.getInstance(properties, new javax.mail.Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication(username, password);
        }
      });
    } else {
      properties.put("mail.smtp.auth", "false");
//      properties.put("mail.smtp.auth", false);
      session = Session.getInstance(properties);
    }
    /**
     * test begin
     */
    session.setDebug(true);
//    properties.put("mail.smtp.ssl.enable", "true");
//    properties.put("mail.smtp.ssl.enable", true);
    /**
     * test begin
     */

    MimeMessage message = new MimeMessage(session);
    if (!StringUtil.isNullOrEmpty(sender)) {
      message.setFrom(new InternetAddress(sender, sender, "UTF-8"));
    }

    if(recipient.contains(";")){
      String[] recipients = recipient.split(";");
      for(int i = 0; i < recipients.length; i++){
        if(!StringUtil.isNullOrEmpty(recipients[i])){
          message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipients[i]));
        }
      }
    }else{
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
    }
    
    
    if (!StringUtil.isNullOrEmpty(bcc)) {
      message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
    }
    
    if (!StringUtil.isNullOrEmpty(cc)) {
      message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
    }

    message.setSubject(subject, "UTF-8");

    // construct the text body part
    MimeBodyPart textBodyPart = new MimeBodyPart();
    textBodyPart.setContent(content, "text/html; charset=UTF-8");

    MimeBodyPart pdfBodyPart = null;
    if (pdf != null && pdfName != null) {
      DataSource textDataSource = new ByteArrayDataSource(pdf, "application/pdf");
      pdfBodyPart = new MimeBodyPart();
      pdfBodyPart.setDataHandler(new DataHandler(textDataSource));
      pdfBodyPart.setFileName(pdfName);
    }

    MimeBodyPart xmlBodyPart = null;
    if (xml != null && xmlName != null) {
      DataSource csvDataSource = new ByteArrayDataSource(xml, "application/zip");
      xmlBodyPart = new MimeBodyPart();
      xmlBodyPart.setDataHandler(new DataHandler(csvDataSource));
      xmlBodyPart.setFileName(xmlName);
    }

    // construct the mime multi part
    MimeMultipart mimeMultipart = new MimeMultipart();
    mimeMultipart.addBodyPart(textBodyPart);

    if (pdfBodyPart != null) {
      mimeMultipart.addBodyPart(pdfBodyPart);
    }
    if (xmlBodyPart != null) {
      mimeMultipart.addBodyPart(xmlBodyPart);
    }

    message.setContent(mimeMultipart);
    
    Transport t = session.getTransport("smtp");
    t.connect(smtpHost,smtpPort,username,password);
    t.sendMessage(message, message.getAllRecipients());
    t.close();
  }

  public static boolean isValidEmailAddress(String email) {
    boolean result = true;
    try {
      InternetAddress emailAddr = new InternetAddress(email);
      emailAddr.validate();
    } catch (AddressException ex) {
      result = false;
    }
    return result;
  }

  public MailSenderUtil setSmtpHost(String smtpHost) {
    this.smtpHost = smtpHost;
    return this;
  }

  public MailSenderUtil setSmtpPort(int smtpPort) {
    this.smtpPort = smtpPort;
    return this;
  }

  public MailSenderUtil setUsername(String username) {
    this.username = username;
    return this;
  }

  public MailSenderUtil setPassword(String password) {
    this.password = password;
    return this;
  }

  public MailSenderUtil setSender(String sender) {
    this.sender = sender;
    return this;
  }

  public MailSenderUtil setSubject(String subject) {
    this.subject = subject;
    return this;
  }

  public MailSenderUtil setContent(String content) {
    this.content = content;
    return this;
  }

  public MailSenderUtil setUseTTL(boolean useTTL) {
    this.useTTL = useTTL;
    return this;
  }

  public MailSenderUtil setRecipient(String recipient) {
    this.recipient = recipient;
    return this;
  }

  public MailSenderUtil setPdf(byte[] pdf) {
    this.pdf = pdf;
    return this;
  }

  public MailSenderUtil setXml(byte[] xml) {
    this.xml = xml;
    return this;
  }

  public MailSenderUtil setPdfName(String pdfName) {
    this.pdfName = pdfName;
    return this;
  }

  public MailSenderUtil setXmlName(String xmlName) {
    this.xmlName = xmlName;
    return this;
  }

  public MailSenderUtil setBcc(String bcc) {
    this.bcc = bcc;
    return this;
  }

  public MailSenderUtil setCc(String cc) {
    this.cc = cc;
    return this;
  }

  public MailSenderUtil setAuth(boolean authRequired) {
    this.auth = authRequired;
    return this;
  }

}
