package com.fit.earsiv.util.parameters;

public class ParamRuntime {

  public static final int LOG_MAX_LENGTH = 2000;
  
  public static final String UTF8_CHARSET = "UTF-8";
  public static final String ASCII_CHARSET = "ASCII";
  
  public enum UserListURLQueryParam {
      GB,
      PK,
      GB_HIST,
      PK_HIST
  }
}
