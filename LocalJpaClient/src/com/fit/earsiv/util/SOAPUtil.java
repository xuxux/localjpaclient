package com.fit.earsiv.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class SOAPUtil {

	static final Logger logger = LoggerFactory.getLogger(SOAPUtil.class);

	public static SOAPMessage invoke(Document doc, String url) {
		try {
			SOAPMessage request = toSOAPMessage(doc);

			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			// Send SOAP Message to SOAP Server
			SOAPMessage soapResponse = soapConnection.call(request, url);
			// Process the SOAP Response
			// printSOAPResponse(soapResponse);
			soapConnection.close();
			return soapResponse;
		}
		catch (Exception e) {
			// System.err.println("Error occurred while sending SOAP Request to Server:" + ExceptionUtils.getStackTrace(e));
			UtilsIO.handleTrivialException(SOAPUtil.class, e);
		}
		return null;
	}

	public static final String prettyPrint(Node xml) throws Exception {
		String strOut;
		Writer out = null;
		try {
			Transformer tf = TransformerFactory.newInstance().newTransformer();
			tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			tf.setOutputProperty(OutputKeys.INDENT, "yes");
			out = new StringWriter();

			tf.transform(new DOMSource(xml), new StreamResult(out));
			strOut = out.toString();
		}
		finally {
			UtilsIO.closeStream(out);
		}
		return strOut;
	}

	public static Document toDocument(SOAPMessage soapMsg) throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
		Source src = soapMsg.getSOAPPart().getContent();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		DOMResult result = new DOMResult();
		transformer.transform(src, result);
		return (Document) result.getNode();
	}

	public static SOAPMessage toSOAPMessage(Document doc) throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
		DOMSource src = new DOMSource(doc);
		MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
		SOAPMessage soapMsg = mf.createMessage();
		soapMsg.getSOAPPart().setContent(src);
		return soapMsg;
	}

}
