package com.fit.earsiv.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class StringUtil {

	private StringUtil() {}

	public static String formatString(String value, Object... params) {
		final MessageFormat temp = new MessageFormat(value);
		return temp.format(params);
	}

	public static String trimLR(String value, String chars) {
		if(null != value) {
			StringBuffer buffer = new StringBuffer(value);
			if(value.startsWith(chars))
				buffer.delete(0, chars.length());
			if(value.endsWith(chars))
				buffer.delete(buffer.length() - chars.length(), buffer.length());
			return buffer.toString();
		}
		return value;
	}

	public static boolean isValidInputWithRegexPattern(String input, String strPattern) {
		try {
			Pattern pattern = Pattern.compile(strPattern);
			if(pattern.matcher(input).matches()) {
				return true;
			}
			return false;
		}
		catch (Exception e) {
			return false;
		}
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		}
		catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static String subStringByChars(String value, String chars) {
		return value.substring(value.indexOf(chars) + chars.length());
	}

	public static String valueAt(String value, int start, int end) {
		return value.substring(start, end);
	}

	public static String findAndCut(StringBuilder value, String regStart, String regEnd) {
		int start = 0, end = 0;
		try {
			start = value.indexOf(regStart);
			end = value.indexOf(regEnd);
			if(start > 0 && end > 0)
				return value.substring(start, end);
			return null;
		}
		finally {
			if(start > 0 && end > 0)
				value.delete(start, end);
		}
	}

	public static String reverseBits(String input) {
		char[] inputChars = input.toCharArray();
		StringBuffer buffer = new StringBuffer();
		for(char c : inputChars) {
			if(c == '0')
				c = '1';
			else if(c == '1')
				c = '0';
			else
				c = '0';
			buffer.append(c);
		}
		return buffer.toString();
	}

	public static boolean isNullOrEmpty(Object value) {
		if(value != null && value.toString().trim().length() > 0) {
			return false;
		}
		return true;
	}

	public static boolean isBytesNullOrEmpty(byte[] data) {
		if(data == null) {
			return true;
		}
		if(data == new byte[0]) {
			return true;
		}

		if(data.length == 0) {
			return true;
		}

		return false;

	}

	public static boolean isEmpty(String value) {
		if(value != null && value.trim().length() > 0)
			return false;
		return true;
	}

	public static String trimAndCut(String value, int size) {
		if(null != value && value.length() > size) {
			String temp = value.trim();
			if(temp.length() <= size) {
				return temp;
			}
			return temp.substring(0, size);
		}
		return value;
	}

	public static String encrypt(String value) {
		if(value != null && value.length() > 5) {
			return value.substring(0, 5) + "**************************";
		}
		return value;
	}

	public static String initCap(String value) {
		final StringBuilder builder = new StringBuilder();
		builder.append(value.substring(0, 1).toUpperCase(Locale.ENGLISH));
		if(value.length() > 1) {
			builder.append(value.substring(1));
		}
		return builder.toString();
	}

	public static String format(String message, Object... params) {
		try {
			return MessageFormat.format(message, params);
		}
		catch (Exception e) {
			return message;
		}
	}

	public static String parseError(Exception e) {
		Throwable cause = e.getCause();
		if(null != cause) {
			return (null != cause.getMessage() ? cause.getMessage() : cause.toString());
		}
		return (null != e.getMessage() ? e.getMessage() : e.toString());
	}

	public static String convertPathString(String path) {
		String result = "";
		try {
			if(path.startsWith("\\"))
				return "\\" + path;
			result = path;
			result = result.replaceAll(Pattern.quote("\\"), "/");
			result = result.replaceAll(Pattern.quote("//"), "/");
			result = result.replaceAll(Pattern.quote("\\/"), "/");
			result = result.replaceAll(Pattern.quote("/\\"), "/");
			result = result.replaceAll(Pattern.quote("//"), "/");
			result = result.replaceAll(Pattern.quote("///"), "/");
			result = result.replaceAll(Pattern.quote("//"), "/");
			result = result.replaceAll(Pattern.quote("///"), "/");
		}
		catch (Exception e) {
			result = path;
		}
		return result;
	}

	public static String traceException(Throwable e) {
		StringBuilder builder = new StringBuilder(512);
		if(null != e.getCause()) {
			builder.append(traceException(e.getCause())).append("\n");
		}
		else {
			builder.append(e.toString()).append("\n");
			for(StackTraceElement ste : e.getStackTrace()) {
				builder.append(ste.toString()).append("\n");
			}
		}
		return builder.toString();
	}

	public static String humanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if(bytes < unit)
			return bytes + " B";

		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	public static String getExtension(String absolutePath) {
		return absolutePath.substring(absolutePath.lastIndexOf('.'));
	}



	public static String[] fileToStringArray(String filePath) {
		List<String> lines = new ArrayList<String>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
			// br = new BufferedReader(new FileReader(filePath));
			String line;
			while((line = br.readLine()) != null) {
				if(lines.size() > Integer.MAX_VALUE) {
					throw new IOException("Input too long.");
				}
				lines.add(line);
			}
		}
		catch (FileNotFoundException e) {
			// System.err.println("XUXUX>>>File not found:" + e.getMessage());
			UtilsIO.handleTrivialException(StringUtil.class, e);
		}
		catch (IOException e) {
			// System.err.println("XUXUX>>>IOException:" + e.getMessage());
			UtilsIO.handleTrivialException(StringUtil.class, e);
		}
		finally {
			try {
				br.close();
			}
			catch (IOException e) {
				// System.err.println("XUXUX>>>IOException while closing BufferedReader:" + e.getMessage());
				UtilsIO.handleTrivialException(StringUtil.class, e);
			}
		}
		String[] array = new String[lines.size()];
		return lines.toArray(array);
	}

	public static String[] streamToStatements(InputStream insertStream) {
		List<String> lines = new ArrayList<String>();

		InputStreamReader is = new InputStreamReader(insertStream);
		BufferedReader br = new BufferedReader(is);
		String read = null;

		try {
			while((read = br.readLine()) != null) {
				if(lines.size() > Integer.MAX_VALUE) {
					throw new IOException("Input too long.");
				}
				lines.add(read);
			}
		}
		catch (IOException e) {
			// System.err.println("XUXUX>>>IOException:" + e.getMessage());
			UtilsIO.handleTrivialException(StringUtil.class, e);
		}
		finally {
			try {
				br.close();
			}
			catch (IOException e) {
				// System.err.println("XUXUX>>>IOException while closing BufferedReader:" + e.getMessage());
				UtilsIO.handleTrivialException(StringUtil.class, e);
			}
		}
		String[] array = new String[lines.size()];
		return lines.toArray(array);
	}


	// query = source=WS&ip=192.168.1.1&VKN_TCKN=0000000000&un=ahmet
	public static Map<String, String> parseHttpQueryString(String query) throws Exception {
		try {
			if(isNullOrEmpty(query)) {
				throw new Exception("Unresolved query string parameter.");
			}
			if(!query.startsWith("?")) {
				query = "?" + query;
			}
			List<NameValuePair> params = URLEncodedUtils.parse(new URI(query), "UTF-8");
			Map<String, String> paramMap = new HashMap<String, String>(5);
			for(NameValuePair param : params) {
				paramMap.put(param.getName(), param.getValue());
			}
			return paramMap;
		}
		catch (Exception e) {
			throw new Exception("Unresolved query string parameter.");
		}
	}

	public static boolean isMailRecipientsValid(String mailTO) {
		try {
			if(isNullOrEmpty(mailTO)) {
				return false;
			}
			String[] mailToArray = mailTO.split(";");
			for(String to : mailToArray) {
				EmailValidator emailValidator = EmailValidator.getInstance(true);
				if(!emailValidator.isValid(to)) {
					return false;
				}
			}
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}

}
