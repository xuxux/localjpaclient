package com.fit.earsiv.util;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

/**
 * 
 * @author ertan
 * 
 */
public class LoggerConfig {

	private static final String logPath = "log/earchive/application.log";
	private static final String logPathArchive = "log/earchive/archive/application-%d{yyyy-MM-dd}.log.gz";
	private static final String logPattern = "%d{HH:mm:ss.SSS, Turkey} [%thread] %-5level %logger{35} - %msg%n";
	private static final int maxFileHistory = 20;

	static {
		try {
			configureLogger();
		}
		catch (Exception e) {
			// System.err.println("Logger Config error; " + e.toString());
			UtilsIO.handleTrivialException(LoggerConfig.class, e);
		}
	}

	public static org.slf4j.Logger getLogger(Class<?> clazz) {
		return LoggerFactory.getLogger(clazz);
	}

	private static void configureLogger() {
		// String userDir = System.getProperty("user.dir");
		Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		loggerContext.reset();

		RollingFileAppender<ILoggingEvent> rfAppender = new RollingFileAppender<ILoggingEvent>();
		rfAppender.setContext(loggerContext);
		rfAppender.setFile(logPath);
		rfAppender.setAppend(true);

		TimeBasedRollingPolicy<ILoggingEvent> tbRollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
		tbRollingPolicy.setContext(loggerContext);
		tbRollingPolicy.setFileNamePattern(logPathArchive);
		tbRollingPolicy.setParent(rfAppender);
		tbRollingPolicy.setMaxHistory(maxFileHistory);
		tbRollingPolicy.start();
		rfAppender.setRollingPolicy(tbRollingPolicy);

		SizeBasedTriggeringPolicy<ILoggingEvent> sizeBasedTriggeringPolicy = new SizeBasedTriggeringPolicy<ILoggingEvent>();
		sizeBasedTriggeringPolicy.setMaxFileSize("1MB");
		rfAppender.setTriggeringPolicy(sizeBasedTriggeringPolicy);

		PatternLayoutEncoder patternLayoutEncoder = new PatternLayoutEncoder();
		patternLayoutEncoder.setContext(loggerContext);
		patternLayoutEncoder.setPattern(logPattern);
		patternLayoutEncoder.start();
		rfAppender.setEncoder(patternLayoutEncoder);
		rootLogger.setLevel(Level.ALL);
		rfAppender.start();
		rootLogger.addAppender(rfAppender);

	}

}
