package com.fit.earsiv.model.userlist.old;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>
 * Java class for userType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identifier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="alias" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="firstCreationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userType", propOrder = {"identifier", "alias", "title", "type", "registerTime", "firstCreationTime"})
public class UserType implements Serializable{


  private static final long serialVersionUID = 1L;
  
  @XmlElement(required = true)
  protected String identifier;
  @XmlElement(required = true)
  protected String alias;
  @XmlElement(required = true)
  protected String title;
  @XmlElement(required = true)
  protected String type;
  @XmlElement(required = true)
  @XmlSchemaType(name = "dateTime")
  protected XMLGregorianCalendar registerTime;
  @XmlElement(required = true)
  @XmlSchemaType(name = "dateTime")
  protected XMLGregorianCalendar firstCreationTime;

  /**
   * Gets the value of the identifier property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getIdentifier() {
    return identifier;
  }

  /**
   * Sets the value of the identifier property.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setIdentifier(String value) {
    this.identifier = value;
  }

  /**
   * Gets the value of the alias property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getAlias() {
    return alias;
  }

  /**
   * Sets the value of the alias property.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setAlias(String value) {
    this.alias = value;
  }

  /**
   * Gets the value of the title property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getTitle() {
    return title;
  }

  /**
   * Sets the value of the title property.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setTitle(String value) {
    this.title = value;
  }

  /**
   * Gets the value of the type property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setType(String value) {
    this.type = value;
  }

  /**
   * Gets the value of the registerTime property.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getRegisterTime() {
    return registerTime;
  }

  /**
   * Sets the value of the registerTime property.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setRegisterTime(XMLGregorianCalendar value) {
    this.registerTime = value;
  }

  /**
   * Gets the value of the firstCreationTime property.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getFirstCreationTime() {
    return firstCreationTime;
  }

  /**
   * Sets the value of the firstCreationTime property.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setFirstCreationTime(XMLGregorianCalendar value) {
    this.firstCreationTime = value;
  }

//  @Override
//  public String toString() {
//    return "identifier:" + getIdentifier() + ", alias:" + getAlias() + ", title:" + getTitle() + ", type:" + getType() + ", registerTime:"
//        + DateUtil.formatShort(getRegisterTime()) + ", firstCreationTime:" + DateUtil.formatShort(getFirstCreationTime());
//
//  }

}
