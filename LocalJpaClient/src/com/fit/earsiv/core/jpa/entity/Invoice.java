package com.fit.earsiv.core.jpa.entity;

/**
 * 07.09.2015 yeni kolon: ORACLE: ALTER TABLE ARC_INVOICE ADD (CUST_INV_ID varchar2(36)); MSSQL:
 * ALTER TABLE ARC_INVOICE ADD CUST_INV_ID nvarchar(36);
 * 
 * CREATE INDEX ARC_INVOICE_IDX1 ON ARC_INVOICE(REPORT_UUID, STATUS_ID, CIDENTIFIER);
 */

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "EARCUSER.ARC_INVOICE", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"INVOICE_ID", "CIDENTIFIER"}),
		@UniqueConstraint(columnNames = {"CUST_INV_ID", "CIDENTIFIER"})})
@NamedQueries( {}
	      )
public class Invoice{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "UUID", length = 36)
	private String UUID;

	@Column(name = "INVOICE_ID", length = 16)
	private String invoiceID;

	@Column(name = "RECEIVER_ID", length = 11)
	private String receiverID;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PROCESS_DATE")
	private Date processDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ISSUE_DATE")
	private Date issueDate;

	@Column(name = "CUST_INV_ID", length = 36)
	private String customerInvId;
	
	@Column(name = "CIDENTIFIER", length = 11)
	private String cidentifier;

	@Column(name = "ENVELOPE_UUID", length = 36)
	private String envUUID;


	public Invoice() {
	}

	public void setUUID(String uUID) {
		this.UUID = uUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setReceiverID(String receiverID) {
		this.receiverID = receiverID;
	}

	public String getReceiverID() {
		return receiverID;
	}

	public String getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(String invoiceID) {
		this.invoiceID = invoiceID;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getIssueDate() {
		return issueDate;
	}


	public String getCustomerInvId() {
		return customerInvId;
	}


	public void setEnvUUID(String envUUID) {
		this.envUUID = envUUID;
	}

	public String getEnvUUID() {
		return envUUID;
	}

	public void setCidentifier(String cidentifier) {
		this.cidentifier = cidentifier;
	}

	public String getCidentifier() {
		return cidentifier;
	}

}
