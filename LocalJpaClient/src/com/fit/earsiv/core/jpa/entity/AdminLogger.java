package com.fit.earsiv.core.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ARC_IC_LOGS")
public class AdminLogger {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "ARC_IC_LOGS")
  @TableGenerator(name = "ARC_IC_LOGS", table = "ARC_IC_LOGS_GEN", pkColumnName = "NAME", pkColumnValue = "ARC_IC_LOGS_GEN", valueColumnName = "VALUE")
  @Column(name = "IDX")
  private long idx;

  @Column(name = "UUID")
  private String uuid;

  @Column(name = "CIDENTIFIER")
  private String cidentifier;

  @Column(name = "TYPE")
  private int type;

  @Column(name = "LOG", length = 2000)
  private String log;

  @Column(name = "INSERT_DATETIME")
  @Temporal(TemporalType.TIMESTAMP)
  private Date insertDateTime;


  // @PrePersist
  // public void trimLog (){
  // if(!StringUtil.isNullOrEmpty(getLog()) && getLog().length() > 1999){
  // setLog(getLog().substring(0, 1999));
  // }
  // }

  public void setIdx(long idx) {
    this.idx = idx;
  }

  public long getIdx() {
    return idx;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getUuid() {
    return uuid;
  }

  public void setCidentifier(String cidentifier) {
    this.cidentifier = cidentifier;
  }

  public String getCidentifier() {
    return cidentifier;
  }

  public void setLog(String log) {
    this.log = log;
  }

  public String getLog() {
    return log;
  }

  public void setType(int type) {
    this.type = type;
  }

  public int getType() {
    return type;
  }

  public void setInsertDateTime(Date insertDateTime) {
    this.insertDateTime = insertDateTime;
  }

  public Date getInsertDateTime() {
    return insertDateTime;
  }

}
