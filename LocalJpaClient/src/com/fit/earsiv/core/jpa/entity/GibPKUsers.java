package com.fit.earsiv.core.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fitcons.einvoice.ds.entity.base.BaseEntity;

/**
 * @author yunus.tastutan@fitcons.com
 * 
 *         Dec 15, 2014 - 9:08:18 PM
 * 
 */

@Entity
@Table(name = "ARC_IC_GIB_USERLIST_PK")
public class GibPKUsers extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private GibPKUserPK id;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "USER_TYPE")
	private String type_;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_CREATION")
	private Date firstCreationTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTER_TIME")
	private Date registerTime;

	public GibPKUsers() {
	}

	public void setId(GibPKUserPK id) {
		this.id = id;
	}

	public GibPKUserPK getId() {
		return id;
	}

	public void setFirstCreationTime(Date firstCreationTime) {
		this.firstCreationTime = firstCreationTime;
	}

	public Date getFirstCreationTime() {
		return firstCreationTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setType_(String type_) {
		this.type_ = type_;
	}

	public String getType_() {
		return type_;
	}

}
