package com.fit.earsiv.core.jpa.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author yunus.tastutan@fitcons.com
 *
 * Dec 15, 2014 - 9:08:18 PM 
 * 
 */

@Embeddable
public class GibPKUserPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="IDENTIFIER", nullable=false, length=11)
	private String identifier;

	@Column(name="ALIAS_NAME", nullable=false,length=250)
	private String alias;

	public GibPKUserPK() {
	}
	
	public GibPKUserPK(String identifier, String alias){
		setIdentifier(identifier);
		setAlias(alias);
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

}
