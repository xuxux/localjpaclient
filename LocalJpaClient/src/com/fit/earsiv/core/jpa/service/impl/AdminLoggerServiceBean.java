package com.fit.earsiv.core.jpa.service.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.fit.earsiv.core.jpa.entity.AdminLogger;
import com.fit.earsiv.core.jpa.service.AdminLoggerService;
import com.fit.earsiv.core.jpa.service.base.AbstractPersistenceService;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class AdminLoggerServiceBean extends AbstractPersistenceService implements AdminLoggerService {

  

  @SuppressWarnings("unchecked")
  public List<AdminLogger> loadLogRecord(String query, Map<String, Object> parameters, int resultSize) {
    try {
      return createDynamicQuery(AdminLogger.class, query, parameters.values().toArray()).setMaxResults(resultSize).getResultList();
    } catch (IllegalStateException ise) {
      throw ise;
    }
  }

}
