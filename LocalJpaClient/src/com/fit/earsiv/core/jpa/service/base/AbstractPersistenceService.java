package com.fit.earsiv.core.jpa.service.base;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TransactionRequiredException;

import com.fit.earsiv.core.jpa.service.IAbstractPersistenceService;

@SuppressWarnings("unchecked")
public abstract class AbstractPersistenceService implements IAbstractPersistenceService {

	@PersistenceContext
	private EntityManager em;

	// @Resource
	// private UserTransaction ut;

	public <T> void persist(T t) {
		try {
			// ut.begin();
			em.persist(t);
			// ut.commit();
		} finally {
			// checkAndRollback();
		}
	}

	public <T> void persistNotTransaction(T t) {
		em.persist(t);
	}

	public <T> void persistAll(List<T> list) {
		try {
			// ut.begin();
			for (T e : list) {
				em.persist(e);
			}
			// ut.commit();
		} finally {
			// checkAndRollback();
		}
	}

	public <T> void persistAllNotTransaction(List<T> list) {
		for (T e : list) {
			em.persist(e);
		}
	}

	public <T> T merge(T t) {
		try {
			// ut.begin();
			T temp = em.merge(t);
			// ut.commit();
			return temp;
		} finally {
			// checkAndRollback();
		}
	}

	public <T> T mergeNotTransaction(T t) {
		T temp = em.merge(t);
		return temp;
	}

	public <T> void mergeAll(List<T> list) {
		try {
			// ut.begin();
			for (T e : list) {
				em.merge(e);
			}
			// ut.commit();
		} finally {
			// checkAndRollback();
		}
	}

	public <T> void mergeAllNotTransaction(List<T> list) {
		for (T e : list) {
			em.merge(e);
		}
	}

	public <T> void remove(Class<?> clazz, Object id) {
		try {
			// ut.begin();
			Object entity = em.getReference(clazz, id);
			em.remove(entity);
			// ut.commit();
		} finally {
			// checkAndRollback();
		}
	}

	public <T> void removeNotTransaction(Class<?> clazz, Object id) {
		Object entity = em.getReference(clazz, id);
		em.remove(entity);
	}

	public int executeUpdateNameQuery(String namedQuery, Object... params) {
		try {
			// ut.begin();
			Query query = em.createNamedQuery(namedQuery);
			setParams(query, params);
			int result = query.executeUpdate();
			// ut.commit();
			return result;
		} finally {
			// checkAndRollback();
		}
	}

	public int executeUpdateNativeQuery(String nativeQuery) {
		try {
			// ut.begin();
			Query query = em.createNativeQuery(nativeQuery);
			int result = query.executeUpdate();
			// ut.commit();
			return result;
		} catch (IllegalStateException  ise) {
			throw ise;
		} catch (TransactionRequiredException tre) {
			throw tre;
		} finally {
			// checkAndRollback();
		}
	}


	public int executeUpdate(String query, Object... params) {
		try {
			Query q = em.createQuery(query);
			setParams(q, params);
			return q.executeUpdate();
		} catch (IllegalStateException  ise) {
			throw ise;
		} catch (TransactionRequiredException tre) {
			throw tre;
		} finally {
			// checkAndRollback();
		}
	}

	public <T> List<T> findAll(Class<T> clazz) {
		return em.createQuery("SELECT o FROM " + clazz.getSimpleName() + " o")
				.getResultList();
	}

	public <T> T findById(Class<T> clazz, Object id) {
		return em.find(clazz, id);
	}

	public <T> T findByIdAndLock(Class<T> clazz, Object id) {
		// em.lock(clazz,LockModeType.READ);
		return em.find(clazz, id);
	}

	public <T> List<T> findByQuery(Class<T> clazz, String jpaQuery, Object... params) {
		return createDynamicQuery(clazz, jpaQuery, params).getResultList();
	}

	public <T> List<T> findByNamedQuery(Class<T> clazz, String namedQuery, Object... params) {
		return createNamedQuery(clazz, namedQuery, params).getResultList();
	}

	public <T> T findByNamedQuerySingle(Class<T> clazz, String namedQuery, Object... params) {
		return (T) createNamedQuery(clazz, namedQuery, params).getSingleResult();
	}

	public <T> List<T> findByNamedQuery(Class<T> clazz, String namedQuery, int readSize,
			Object... params) {
		return createNamedQuery(clazz, namedQuery, params).setMaxResults(readSize)
				.getResultList();
	}

	public <T> List<T> findByNamedNativeQuery(Class<T> clazz, String namedNativeQuery,
			Object... params) {
		return createNamedNativeQuery(clazz, namedNativeQuery, params).getResultList();
	}

	public <T> T findByNamedNativeQuerySingle(Class<T> clazz, String namedNativeQuery,
			Object... params) {
		return (T) createNamedNativeQuery(clazz, namedNativeQuery, params)
				.getSingleResult();
	}

	public <T> List<T> findByNamedNativeQuery(Class<T> clazz, String namedNativeQuery,
			int readSize, Object... params) {
		return createNamedNativeQuery(clazz, namedNativeQuery, params).setMaxResults(
				readSize).getResultList();
	}

	public <T> T findByQuerySingle(Class<T> clazz, String jpaQuery, Object... params) {
		return (T) createDynamicQuery(clazz, jpaQuery, params).getSingleResult();
	}

	public Query createDynamicQuery(Class<?> clazz, String jpaQuery, Object... params) {
		Query query = em.createQuery(jpaQuery);
		setParams(query, params);
		return query;
	}

	public Query createNamedQuery(Class<?> clazz, String namedQuery, Object... params) {
		Query query = em.createNamedQuery(namedQuery);
		setParams(query, params);
		return query;
	}

	public Query createNamedNativeQuery(Class<?> clazz, String namedNativeQuery,
			Object... params) {
		Query query = em.createNamedQuery(namedNativeQuery);
		setParams(query, params);
		return query;
	}

	public void setParams(Query query, Object... params) {
		int index = 1;
		for (Object param : params) {
			if (param instanceof java.sql.Date) {
				query.setParameter(index++, (java.sql.Date) param,
						TemporalType.DATE);
			} else if (param instanceof java.sql.Timestamp) {
				query.setParameter(index++, (java.sql.Timestamp) param,
						TemporalType.TIMESTAMP);
			} else if (param instanceof java.util.Date) {
				query.setParameter(index++, (java.util.Date) param,
						TemporalType.TIMESTAMP);
			} else if (param instanceof java.util.Calendar) {
				query.setParameter(index++, (java.util.Calendar) param,
						TemporalType.TIMESTAMP);
			} else {
				query.setParameter(index++, param);
			}

		}
	}

	public EntityManager getEntityManager() {
		return em;
	}

	// public UserTransaction getTransaction() {
	// return ut;
	// }

	public void flush() {
		em.flush();
	}

	// public void checkAndRollback() {
	// try {
	// if (ut.getStatus() == Status.STATUS_ACTIVE) {
	// ut.rollback();
	// }
	// } catch (Exception e) {}
	// }

}
