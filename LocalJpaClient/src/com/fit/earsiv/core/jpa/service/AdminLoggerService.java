package com.fit.earsiv.core.jpa.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.fit.earsiv.core.jpa.entity.AdminLogger;

@Local
public interface AdminLoggerService extends IAbstractPersistenceService {

  public abstract List<AdminLogger> loadLogRecord(String query, Map<String, Object> parameters, int resultSize);

}
