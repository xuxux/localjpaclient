/**
 * 
 */
package com.fit.earsiv.core.jpa.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * @author fsoylemez
 * @author murat.demir
 * 
 */
public interface IAbstractPersistenceService {

	public <T> void persist(T t);

	public <T> void persistNotTransaction(T t);

	public <T> void persistAll(List<T> list);

	public <T> void persistAllNotTransaction(List<T> list);

	public <T> T merge(T t);

	public <T> T mergeNotTransaction(T t);

	public <T> void mergeAll(List<T> list);

	public <T> void mergeAllNotTransaction(List<T> list);

	public <T> void remove(Class<?> clazz, Object id);

	public <T> void removeNotTransaction(Class<?> clazz, Object id);

	public int executeUpdateNameQuery(String namedQuery, Object... params);

	public int executeUpdateNativeQuery(String nativeQuery);

	public <T> List<T> findAll(Class<T> clazz);

	public <T> T findById(Class<T> clazz, Object id);

	public <T> List<T> findByQuery(Class<T> clazz, String jpaQuery, Object... params);

	public <T> List<T> findByNamedQuery(Class<T> clazz, String namedQuery, Object... params);

	public <T> T findByNamedQuerySingle(Class<T> clazz, String namedQuery, Object... params);

	public <T> List<T> findByNamedQuery(Class<T> clazz, String namedQuery, int readSize,
			Object... params);

	public <T> T findByQuerySingle(Class<T> clazz, String jpaQuery, Object... params);

	public Query createDynamicQuery(Class<?> clazz, String jpaQuery, Object... params);

	public Query createNamedQuery(Class<?> clazz, String namedQuery, Object... params);

	public void setParams(Query query, Object... params);

	public EntityManager getEntityManager();

	public <T> T findByNamedNativeQuerySingle(Class<T> clazz, String namedNativeQuery,
			Object... params);

	public int executeUpdate(String query, Object... params);

	// public UserTransaction getTransaction();

	public void flush();

	// public void checkAndRollback();


}
