/**
 * 
 */
package com.fit.earsiv.handler;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

/**
 * @author yunus.tastutan@fitcons.com
 * 
 *         Feb 21, 2015 - 1:42:28 PM
 */
public class MyValidationEventHandler implements ValidationEventHandler {
	private StringBuilder builder;
	private boolean error = false;

	public boolean handleEvent(ValidationEvent event) {
		builder = new StringBuilder();
//		getBuilder().append("EVENT");
		getBuilder().append("\nSEVERITY:  " + event.getSeverity());
		getBuilder().append("\nMESSAGE:  " + event.getMessage());
//		getBuilder().append("\nLINKED EXCEPTION:  " + event.getLinkedException());
		getBuilder().append("\nLOCATOR:");
		getBuilder().append("    LINE NUMBER:" + event.getLocator().getLineNumber());
		getBuilder().append("    COLUMN NUMBER:" + event.getLocator().getColumnNumber());
//		getBuilder().append("    OFFSET:  " + event.getLocator().getOffset());
//		getBuilder().append("    OBJECT:  " + event.getLocator().getObject());
//		getBuilder().append("    NODE:  " + event.getLocator().getNode());
//		getBuilder().append("    URL:  " + event.getLocator().getURL());
		setError(true);
		return true;
	}

	public void setBuilder(StringBuilder builder) {
		this.builder = builder;
	}

	public StringBuilder getBuilder() {
		return builder;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isError() {
		return error;
	}
}
