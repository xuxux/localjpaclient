package com.fitcons.einvoice.ds.entity.base.raw;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fitcons.einvoice.ds.entity.base.BaseEntity;


@MappedSuperclass
public class ENTInvRawSBD extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 9188237817L;

  @Id
  @Column(name = "UUID", length = 36)
  private String uuid;

  @Column(name = "SENDER_ID", length = 11)
  private String senderId;

  @Column(name = "SENDER_IDENTIFIER", length = 200)
  private String senderIdentifier;

  @Column(name = "RECEIVER_ID", length = 11)
  private String receiverId;

  @Column(name = "RECEIVER_IDENTIFIER", length = 200)
  private String receiverIdentifier;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "INSERT_DATETIME")
  private Date insertDatetime;

  @Lob
  @Column(name = "BINARY_DATA")
  private byte[] binaryData;

  @Column(name = "TYPE", length = 30)
  private String type;

  private BigDecimal state;

  @Column(name = "RETRY_COUNT")
  private BigDecimal retryCount;

  @Column(name = "ERROR", length = 4000)
  private String error;

  @Column(name = "FROM_ARCHIVE")
  private BigDecimal fromArchive;


  public ENTInvRawSBD() {}

  public String getSenderId() {
    return senderId;
  }

  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }

  public String getSenderIdentifier() {
    return senderIdentifier;
  }

  public void setSenderIdentifier(String senderIdentifier) {
    this.senderIdentifier = senderIdentifier;
  }

  public String getReceiverId() {
    return receiverId;
  }

  public void setReceiverId(String receiverId) {
    this.receiverId = receiverId;
  }

  public String getReceiverIdentifier() {
    return receiverIdentifier;
  }

  public void setReceiverIdentifier(String receiverIdentifier) {
    this.receiverIdentifier = receiverIdentifier;
  }

  public byte[] getBinaryData() {
    return this.binaryData;
  }

  public void setBinaryData(byte[] binaryData) {
    this.binaryData = binaryData;
  }

  public Date getInsertDatetime() {
    return this.insertDatetime;
  }

  public void setInsertDatetime(Date insertDatetime) {
    this.insertDatetime = insertDatetime;
  }

  public BigDecimal getState() {
    return this.state;
  }

  public void setState(BigDecimal state) {
    this.state = state;
  }

  public String getUuid() {
    return this.uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public BigDecimal getRetryCount() {
    return retryCount;
  }

  public void setRetryCount(BigDecimal retryCount) {
    this.retryCount = retryCount;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public void setFromArchive(BigDecimal fromArchive) {
    this.fromArchive = fromArchive;
  }

  public BigDecimal getFromArchive() {
    return fromArchive;
  }

}
