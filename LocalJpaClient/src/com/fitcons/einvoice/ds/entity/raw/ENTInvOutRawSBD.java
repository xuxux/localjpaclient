package com.fitcons.einvoice.ds.entity.raw;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fitcons.einvoice.ds.entity.base.raw.ENTInvRawSBD;


/**
 * The persistent class for the INV_OUT_RAW_SBD database table.
 * 
 */
@Entity
@Table(name = "INV_OUT_RAW_SBD")
@NamedQueries( {@NamedQuery(name = "ENTInvOutRawSBD.getEnvBySender", query = "SELECT t FROM ENTInvOutRawSBD t WHERE t.senderIdentifier=?1 AND t.senderId=?2 AND t.insertDatetime>=?3 AND t.insertDatetime<=?4")})
public class ENTInvOutRawSBD extends ENTInvRawSBD {

  private static final long serialVersionUID = 621983921L;

}
