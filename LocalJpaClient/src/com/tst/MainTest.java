package com.tst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import com.fit.earsiv.core.jpa.entity.GibPKUsers;
import com.fit.earsiv.core.jpa.entity.Invoice;
import com.fit.earsiv.model.userlist.old.Resp;
import com.fit.earsiv.model.userlist.old.UserType;
import com.fit.earsiv.util.DateUtil;
import com.fit.earsiv.util.GenericMarshallUnmarshallUtil;
import com.fit.earsiv.util.ZipUtil;
import com.xuxux.utils.commons.MyDateUtil;
import com.xuxux.utils.files.MyFileUtil;

public class MainTest {

	private final static String INPUT_FILE_PATH = "C:/Users/xuxux/Desktop/tmp/cust_inv_IDs.txt";
	private final static String OUTPUT_FILE_PATH = "C:/Users/xuxux/Desktop/tmp/matchings.csv";
	private final static String CIDENTIFIER = "5200043963";
	private final static String CSV_DELIMETER = ";";

	public static void main(String[] args) throws IOException {
		int counter = 0;
		BufferedReader br;
		FileReader fr;
		File file;
		String custInvID = null;
		file = new File(INPUT_FILE_PATH);
		fr = new FileReader(file);
		br = new BufferedReader(fr);
		// Create the EntityManager
		EntityManagerFactory factory =
				Persistence.createEntityManagerFactory("PU_E_ARCHIVE_LOCAL");
		EntityManager em = factory.createEntityManager();
		Query query =
				em
						.createQuery("SELECT inv FROM Invoice inv WHERE inv.customerInvId = ?1 AND inv.cidentifier = ?2");

		while ((custInvID = br.readLine()) != null) {
			counter++;
			try {
				if (counter % 1000 == 0) {
					System.out.println(counter);
				}
				Invoice inv =
						getInvByCustInvID_Cidentifier(custInvID,
								CIDENTIFIER, query);
				if (inv != null) {
					StringBuilder sb = new StringBuilder();
					sb.append(inv.getCustomerInvId()).append(CSV_DELIMETER)
							.append(inv.getInvoiceID()).append(
									CSV_DELIMETER).append(
									inv.getUUID()).append(
									CSV_DELIMETER).append(
									DateUtil.formatMid(inv.getProcessDate()))
							.append(CSV_DELIMETER).append(
									'\n');

					MyFileUtil.appendStringToFile(sb.toString(),
							OUTPUT_FILE_PATH);
				} else {
					StringBuilder sb = new StringBuilder();
					sb.append(custInvID).append(CSV_DELIMETER).append("N/A")
							.append(CSV_DELIMETER).append("N/A")
							.append(CSV_DELIMETER).append("N/A")
							.append('\n');
					MyFileUtil.appendStringToFile(sb.toString(),
							OUTPUT_FILE_PATH);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}



	}

	public static Invoice getInvByCustInvID_Cidentifier(String custInvID, String cidentifier,
			Query q) {
		try {
			q.setParameter(1, custInvID);
			q.setParameter(2, cidentifier);

			// query = mainTest.setParams(query, new Date());

			Invoice inv = (Invoice) q.getSingleResult();
			return inv;
		} catch (javax.persistence.NoResultException nre) {
			System.err.println("Could Not find: " + custInvID);
		}
		return null;

	}


	public static void getEinvUserList() throws DatatypeConfigurationException, JAXBException,
			UnsupportedEncodingException, IOException {
		try {

			// Create the EntityManager
			EntityManagerFactory factory =
					Persistence
							.createEntityManagerFactory("PU_E_ARCHIVE_LOCAL");
			EntityManager em = factory.createEntityManager();

			Query query = em.createQuery("SELECT ul FROM GibPKUsers ul");

			// query = mainTest.setParams(query, new Date());

			List<GibPKUsers> gibPkUsers = query.getResultList();
			System.out.println(gibPkUsers.size());
			UserType userType;
			Resp resp = new Resp();
			for (int i = 0; i < gibPkUsers.size(); i++) {
				userType = new UserType();
				userType.setIdentifier(gibPkUsers.get(i).getId().getIdentifier());
				userType.setAlias(gibPkUsers.get(i).getId().getAlias());
				userType.setTitle(gibPkUsers.get(i).getTitle());
				userType.setType(gibPkUsers.get(i).getType_());
				userType.setFirstCreationTime(DateUtil.toXmlDateTime(gibPkUsers
						.get(i).getFirstCreationTime()));
				userType.setRegisterTime(DateUtil.toXmlDateTime(gibPkUsers.get(i)
						.getRegisterTime()));
				resp.getUser().add(userType);

			}
			String xml = GenericMarshallUnmarshallUtil.marshall(resp);
			System.out.println("xml verinin byte uzunluğu:" + xml.getBytes().length);
			byte[] zipOLDData = ZipUtil.zipOLD("GIBPKUsers.xml", xml.getBytes("UTF-8"));
			byte[] zipData = ZipUtil.zip("GIBPKUsers.xml", xml.getBytes("UTF-8"));
			System.out.println("zipData:" + zipData.length + " - zipOLDData:"
					+ zipOLDData.length);
			if (zipData.length == zipOLDData.length) {
				System.out.println("SUCCESS!");
			} else {
				System.err.println("FAIL!!!");
			}
			MyFileUtil.writeByteArrayToFile(zipOLDData, "D:/TMP/OLDuserlistOut.zip");
			MyFileUtil.writeByteArrayToFile(zipData, "D:/TMP/userlistOut.zip");
		} catch (IllegalStateException ise) {
			throw ise;
		} catch (IllegalArgumentException iae) {
			throw iae;
		}
	}

	public Query setParams(Query query, Object... params) {
		int index = 1;
		for (Object param : params) {
			if (param instanceof java.sql.Date) {
				query.setParameter(index++, (java.sql.Date) param,
						TemporalType.DATE);
			} else if (param instanceof java.sql.Timestamp) {
				query.setParameter(index++, (java.sql.Timestamp) param,
						TemporalType.TIMESTAMP);
			} else if (param instanceof java.util.Date) {
				query.setParameter(index++, (java.util.Date) param,
						TemporalType.DATE);
			} else if (param instanceof java.util.Calendar) {
				query.setParameter(index++, (java.util.Calendar) param,
						TemporalType.TIMESTAMP);
			} else {
				query.setParameter(index++, param);
			}

		}
		return query;
	}

}
